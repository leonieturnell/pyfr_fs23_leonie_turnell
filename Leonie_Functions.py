#Semesterarbeit 1a, Aufgabe zu Funktionen in Python

"""
Funktion für den arithmetischen Mittelwert: Das arithmetische Mittel beschreibt den statistischen Durchschnittswert.
"""
x=[1, 2, 3, 4]
def ar_mean(x):
    total = sum(x) #Summe der Liste x
    anzahl = len(x) #Anzahl n der Liste x
    mean = total / anzahl
    return mean

"""
Funktion für den geometrischen Mittelwert: Das geometrische Mittel, auch mittlere Proportionale genannt, ist ein Lagemaß von quantitativen Beobachtungswerten der deskriptiven Statistik. 
Man erhält ihn durch die Berechnung der n-ten Wurzel aus dem Produkt der betrachteten positiven Zahlenwerte
"""

def geo_mean(x):
    produkt = 1
    for zahl in x: #Erstellung eines for-loops um das Produkt aller elemente in der Liste x zu erhalten
        produkt *= zahl #Produkt wird berechnet indem jede Zahl der Liste durchgegangen und aufmultipliziert wird.
    anzahl = len(x) #Anzahl der elemente in x, damit wir die n-te Wurzel ziehen können
    geo_mean = produkt**(1/anzahl) #Die n-te Wurzel wird vom Produkt gezogen um den geometrischen Mittelwert zu erhalten.
    return geo_mean

"""
Funktion für harmonisches Mittel: 
Um das harmonische Mittel zu berechnen, dividiert man die Anzahl der Beobachtungswerte 
durch die Summe der Kehrwerte der Beobachtungswerte von 1/x1 bis 1/xn.
"""

def har_mean(x):
    anzahl = len(x) #Anzahl der Eolemente in Liste x
    kehrwert = [1/zahl for zahl in x] # Kehrzahlen von Ganzzahlen = 1/zahl selber, dazu wurde eine List Comprehension erstellt, welcher durch die Elemente der Liste x geht und für alle ein Kehrwert erstellt.
    har_mean = anzahl / sum(kehrwert) #Anzahl der Elemente wird duch die Summe aus dessen Kehrwert erstellt.
    return har_mean


"""
Funktion für Median, unterscheidung zwischen gerader und ungerader Anzahl Elementen aus Liste x.
Achtung! Indexzahlen beginnen bei Python bei [0] und nicht bei [1]!
Verwendung der Formel für den Median.
"""

def median(x):
    x.sort() #Sortierung der Liste, damit man die Median-Berechnung gemäss Indexzahlen machen kann.
    n = len(x) #Anzahl Elemente aus Liste x, so weiss man, ob man die Berechnung für n= gerade oder n=ungerade nehmen muss.
    if n % 2 == 0: # n = gerade Anzahl; hierbei findet man heraus, ob die Anzahl der Elemente aus x gerade oder ungerade ist.
        x1 = n // 2
        x2 = x1 - 1 # -1 da die Indexzahlen bei 0 beginnen
        median = (x[x2] + x[x1]) / 2
    else: # n = ungerade Anzahl
        x3 = (n + 1) // 2
        median = x[x3-1] #-1 da die Indexzahlen bei 0 beginnen
    return median

#Funktion für Modus - welches Element kommt kommt am meisten in x vor?

def modus(x):
    freq = {} #Platzhalter für die Berechnung der Anzahl der einzelnen Elemente in x
# Berechnung der Anzahl jeder Zahl in der Liste
    for element in x:
        if element in freq:
            freq[element] += 1 #wenn es mehrmals vorkommt die Summe der Anzahl des Elements addieren
        else:
            freq[element] = 1 #wenn das Element nur einmal vorkommt, dann zähle 1

# Herausfinden der Zahl mit der höchsten Frequenz
    max = 0 #Definition von Maximum, wird im loop überschrieben
    for element, anzahl in freq.items(): #Erstellung eines For-Loops, um die Anzahl der elemente in der Liste freq abzurufen. Zugriff auf die Elemente in Freq mit freq.items
        if anzahl > max: #Maximum Anzahl der Elemente in x ausfindig machen, max wird hierdurch durch die Anzahl überschrieben, wenn die Anzahl grösser als max ist.
            modus = element
            max = anzahl
    return modus #Ausgabe der maximalen Anzahl, nachdem man durch die Liste durchgeloopt ist :).

"""
Funktion für Varianz
Ich verwende die Offizielle Formel für die Varianz.
1. Mittelwert aller Beobachtungswerte berechnen - dazu benutze ich meine Funktion ar_mean()
Abweichungen der Beobachtungswerte vom Mittelwert bestimmen.
Abweichungen (aus Schritt 2) quadrieren.
Quadrierte Abweichungen (aus Schritt 3) addieren.
Summe (aus Schritt 4) durch Gesamtanzahl der Beobachtungen – 1 teilen.
"""
def var(x):
    mean = ar_mean(x) #Schritt 1
    n = len(x)
    y = 0
    for element in x:
        y += (element - mean) ** 2 #Schritt 2 + 3, einzelne quadrierte Werte mit for - Loop aufsummieren
    var = y / (n - 1) #Schritt 4
    return var

"""Funktion für Standardabweichung - die Standardabweichung ist die Wurzel der Varianz, dazu vernwende ich meine Varianz-Funktion
 und ziehe die Wurzel
"""

def std(x):
    varianz = var(x)
    std = varianz ** 0.5 #Wurzel ziehen mit ** 0.5
    return std

"""
Funktion für Variationskoeffizient:
Der Variationskoeffizient ist definiert als das Verhältnis der Standardabweichung zum arithmetischen Mittelwert
ich verwende meine bereits erstellten Funktionen
"""

def var_coef(x):
    mean = ar_mean(x)
    sd = std(x)
    var_coef = sd / mean
    return var_coef

